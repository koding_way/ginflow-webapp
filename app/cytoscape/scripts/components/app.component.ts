import { Component, Input, ViewChild }	from '@angular/core';
import { SELECT_DIRECTIVES }			from 'ng2-select/ng2-select';

import { GraphComponent }				from './graph.component';

import { JsonService }					from '../services/json.service';

import { TypeControl }					from '../type-control';
import { TypeSelect }					from '../type-select';
import { GinflowResult }				from '../ginflow-result';
import { Service }						from '../service';

@Component({
	selector: 'cytoscape-app',
	templateUrl: 'app/cytoscape/views/app.component.html',
	styleUrls: ['app/cytoscape/styles/app.component.css'],
	directives: [GraphComponent]
})

/**
 * @namespace Components
 */

/**
 * @class Components.AppComponent
 * @classdesc Main Angular component for the Cytoscape graph
 */
export class AppComponent {

	/**
	 * The current selected {@link Service}.
	 * If none is selected, this variable is a Service with id 0.
	 * @member {Service} Components.AppComponent#selectedService
	 * @see Service
	 */ 
	selectedService: Service = new Service(0);

	/*
	 * The current selected edge.
	 * If none is selected, this variable is an empty object.
	 * @member {any} Components.AppComponent#selectedEdge
	 */
	selectedEdge: any = {};

	/**
	 * Boolean used for Angular ngIf.
	 * True - Hide the form which display the data of services.
	 * False - Show the form which display the data of services.
	 * @member {boolean} Components.AppComponent#hiddenNode
	 */
	hiddenNode : boolean;

	/**
	 * Boolean used for Angular ngIf.
	 * True - Hide the form which display the data of links.
	 * False - Show the form which display the data of links.
	 * @member {boolean} Components.AppComponent#hiddenEdge
	 */
	hiddenEdge : boolean;

	/**
	 * True - Set some fields and buttons of the {@link Components.AppComponent} non-editable/disabled.
	 * False - Default value.
	 * @member {boolean} Components.AppComponent#disableUpdate
	 */
	disableUpdate: boolean = false;

	/**
	 * True - The workflow has finished
	 * False - Default value. The workflow is not launched or is not executed entirely.
	 * @member {boolean} Components.AppComponent#onEndCalled
	 */
	onEndCalled: boolean = false;

	/**
	 * Boolean used for Angular ngIf.
	 * True - The field to import JSON is displayed.
	 * False - Default value. The field to import JSON is hidden.
	 * @member {boolean} Components.AppComponent#loadJSON
	 */
	loadJSON: boolean = false;

	/**
	 * Boolean used for Angular ngIf.
	 * True - The service has a result.
	 * False - Default value. The service has not been executed yet, no result available
	 * @member {boolean} Components.AppComponent#serviceOk
	 */
	serviceOk: boolean = false;

	/**
	 * Boolean used for Angular ngIf.
	 * True - The button "Export to JSON" has been pressed. The form JSON preview is displayed.
	 * False - Default value. The form JSON preview is hidden.
	 * @member {boolean} Components.AppComponent#exportJSON
	 */
	exportJSON: boolean;

	/**
	 * Boolean used for Angular ngIf.
	 * True - Default value. The JSON form is hidden.
	 * False - The JSON form is displayed.
	 * @member {boolean} Components.AppComponent#hideJSONForm
	 */
	hideJSONForm: boolean;

	/**
	 * Boolean used for Angular ngIf.
	 * True - The JSON form is submitted.
	 * False - Default value. The JSON form is not submitted.
	 * @member {boolean} Components.AppComponent#submittedJSON
	 */
	submittedJSON: boolean = false;

	/**
	 * {@link Enums.TypeSelect} enum instance which allows to know which is the current event.
	 * @member {Enums.TypeSelect} Components.AppComponent#typeSelection
	 * @see Enums.TypeSelect
	 */
	typeSelection : TypeSelect;

	/**
	 * {@link GinflowResult} of the current service selected.
	 * @member {GinflowResult} Components.AppComponent#selectedGinflowResult
	 * @see GinflowResult
	 */
	selectedGinflowResult : GinflowResult;

	/**
	 * Object representing the imported data.
	 * @member {any} Components.AppComponent#importData
	 */
	importData: any = {};

	/**
	 * Object representing the exported data.
	 * @member {any} Components.AppComponent#exportData
	 */
	exportData: any = {};

	/**
	 * Function called when a node or a edge in the graph is selected (or deselected).
	 * Called when {@link Components.GraphComponent#select} is emitting an event.
	 * @method Components.AppComponent#onSelect
	 * @param {any} event The event (click) which is a json object containing node of edge informations
	 */
	onSelect(event : any) {
		this.submittedJSON = false;
		this.typeSelection = event.value.type;
		switch (this.typeSelection) {

			// If we select a node
			case TypeSelect.NodeSelection:
				this.hiddenNode = false;
				this.hiddenEdge = true;

				/*
				 * We display the informations of this service.
				 * Catch event sent by the child a.k.a. CytoscapeGraphComponent
				 */
				this.selectedService = event.value.service;
				
				// If the result of a service is available
				if(event.value.result != undefined) {
					// We can display informations about the resulted service
					this.serviceOk = true;
			    this.selectedGinflowResult = event.value.result;
				}

				// If we reach the end of the workflow, then all services have been executed
			  // Catch the result of the selected service sent by the child (CytoscapeGraphComponent)
				break;

			// If we deselect a node
			case TypeSelect.NodeDeselection:
				// We hide informations about nodes and edges
				this.hiddenNode = true;
				this.hiddenEdge = true;
				break;
			
			// If we select an edge
			case TypeSelect.EdgeSelection:
				this.hiddenNode = true;
				this.hiddenEdge = false;

				/*
				 * We display the informations of this edge.
				 * Catch event sent by the child a.k.a. CytoscapeGraphComponent
				 */
				this.selectedEdge = event.value;
				break;

			// If we deselect an edge
			case TypeSelect.EdgeDeselection:
			// We hide informations about nodes and edges
				this.hiddenNode = true;
				this.hiddenEdge = true;
				break;
		}
	}

	/**
	 * Function called in the template when selecting the radio button data dependency.
	 * @method Components.AppComponent#onChangeData
	 */
	onChangeData() {
		this.selectedEdge.type_control = TypeControl.Data;
	}

	/**
	 * Function called in the template when selecting the radio button control dependency.
	 * @method Components.AppComponent#onChangeControl
	 */
	onChangeControl() {
		this.selectedEdge.type_control = TypeControl.Control;
	}

	/**
	 * Function called when clicking on launch button (also clear graph button).
	 * Called when {@link Components.GraphComponent#launch} is emitting an event.
	 * @method Components.AppComponent#onLaunch
	 * @param {any} event The event (click) which will trigger the action
	 */
	onLaunch(event : any) {
		this.hiddenEdge = true;
		this.hiddenNode = true;
	}

	/**
	 * Function called when the workflow ending.
	 * Disable any update.
	 * Called when {@link Components.GraphComponent#end} is emitting an event.
	 * @method Components.AppComponent#onEnd
	 * @param {any} event The event (click) which will trigger the action
	 */
	onEnd(event : any) {
		this.disableUpdate = true;
		this.onEndCalled = true;
	}

	/**
	 * When a click is detected in the button creation graph/load json.
	 * Called when {@link Components.GraphComponent#toggle} is emitting an event.
	 * @method Components.AppComponent#onToggle
	 * @param {any} event The event (click) which will trigger the action
	 */
	onToggle(event : any) {
		this.loadJSON = event.value.load;
		this.exportJSON = event.value.exportJSON;
	}

	/**
	 * Called when submitting the JSON plain-text in the form.
	 * Create nodes with :
	 * 1) data :
	 *		a) an id
	 *		b) a {@link Service} instance
	 * 2) position
	 * Then update their dependencies (src, dst, ...).
	 * And finally, create edges from these dependencies.
	 * @method Components.AppComponent#onSubmitJSON
	 * @param {any} data The data received from the event
	 */
	onSubmitJSON(data : any) {
		// The string contained in the form export JSON is converted into JSON
		this.importData = JSON.parse(data);
		this.submittedJSON = true;
	}

	/**
	 * Called when exporting graph to JSON.
	 * Called when {@link Components.GraphComponent#exports} is emitting an event.
	 * @method Components.AppComponent#onJSONExport
	 * @param {any} event The event (click) which will trigger the action
	 */
	onJSONExport(event : any) {
		this.exportJSON = event.value.isExportJSON;
		this.exportData = event.value.data;
		this.hideJSONForm = event.value.isJSONFormHidden;
		this.loadJSON = event.value.isImportJSON;

	}
}
