export class Rebranching {
	
	/**
	 * @member {string[]} Rebranching#supervised
	 */
	supervised: string[];

	/**
	 * @member {any} Rebranching#updateSrc
	 */
	updateSrc: any;

	/**
	 * @member {any} Rebranching#updateDst
	 */
	updateDst: any;

	/**
	 * @member {any} Rebranching#updateSrcControl
	 */
	updateSrcControl: any;

	/**
	 * @member {any} Rebranching#updateDstControl
	 */
	updateDstControl: any;

	/**
	 * Constructor.
	 * @class Rebranching
	 * @classdesc Class which will manage alternatives setted on the fly during the execution.
	 * @param {string} supervised The array of services (names) which belongs to the current supervised group
	 * @param {any} updateSrc The object which contains information about adaptation for src dependencies
	 * @param {any} updateDst The object which contains information about adaptation for dst dependencies
	 * @param {any} updateSrcControl The object which contains information about adaptation for srcControl dependencies
	 * @param {any} updateDstControl The object which contains information about adaptation for dstControl dependencies
	 */
	constructor(supervised: string[],
				updateSrc: any,
				updateDst: any,
				updateSrcControl: any,
				updateDstControl: any) {
		this.supervised = supervised;
		this.updateSrc = updateSrc;
		this.updateDst = updateDst;
		this.updateSrcControl = updateSrcControl;
		this.updateDstControl = updateDstControl;
	}
}