export class Service {
	/**
	 * @member {number} Service#id
	 */
	id: number;

	/**
	 * @member {string} Service#name
	 */
	name: string;

	/**
	 * The command of the service.
	 * @member {string} Service#srv
	 */
	srv: string;

	/**
	 * The parameter(s) of the service command.
	 * @member {string[]} Service#in
	 */
	in: string[];

	/**
	 * The alternative group of the service.
	 * Used for adaptiveness.
	 * @member {number} Service#alt
	 */
	alt: number;

	/**
	 * The supervised group of the service.
	 * Used for adaptiveness.
	 * @member {number} Service#sup
	 */
	sup: number;

	/**
	 * Sources dependencies of the service.
	 * @member {Service[]} Service#src
	 */
	src: Service[];

	/**
	 * Destinations dependencies of the service.
	 * @member {Service[]} Service#dst
	 */
	dst: Service[];

	/**
	 * Sources (control) dependencies of the service.
	 * @member {Service[]} Service#srcControl
	 */
	srcControl: Service[];

	/**
	 * Destinations (control) dependencies of the service.
	 * @member {Service[]} Service#dstControl
	 */
	dstControl: Service[];

	/**
	 * @member Service#description
	 */
	description: string;

	/** 
	 * Construct a Service object just by giving the id.
	 * If you want to provide other parameters, use the setters.
	 * @class Service
	 * @classdesc Represents the description of a service in the workflow
	 * @param {number} id
	 */
	constructor(id : number) {
		this.id = id;
		this.name = this.id.toString();
		this.srv = '';
		this.in = [];
		this.alt = undefined;
		this.sup = undefined;
		this.src = [];
		this.dst = [];
		this.srcControl = [];
		this.dstControl = [];
		this.description = 'Service with id' + id.toString();
	}

	/**
	 * Get the id of the service.
	 * @method Service#getId
	 * @returns {number} The id
	 */
	public getId() : number {
		return this.id;
	}

	/**
	 * Get the name of the service.
	 * @method Service#getName
	 * @returns {string} The name
	 */
	public getName() : string {
		return this.name;
	}

	/**
	 * Set a new name for a service.
	 * @method Service#setName
	 * @param {string} newName The new name for the service
	 * @returns {Service} The updated Service
	 */
	public setName(newName : string) : Service {
		this.name = newName;
		return this;
	}

	/**
	 * Get the srv of the service.
	 * @method Service#getSrv
	 * @returns {string} The srv
	 */
	public getSrv() : string {
		return this.srv;
	}

	/**
	 * Set a new srv for a service.
	 * @method Service#setSrv
	 * @param {string} newSrv The new srv for the service
	 * @returns {Service} The updated Service
	 */
	public setSrv(newSrv : string) : Service {
		this.srv = newSrv;
		return this;
	}

	/**
	 * Get the in parameter of the service.
	 * @method Service#getIn
	 * @returns {string[]} The in parameter
	 */
	public getIn() : string[] {
		return this.in;
	}

	/**
	 * Set a new in parameter for a service.
	 * @method Service#setIn
	 * @param {string[]} newIn The new in parameter for the service
	 * @returns {Service} The updated Service
	 */
	public setIn(newIn : string[]) : Service {
		this.in = newIn;
		return this;
	}

	/**
	 * Get the alt group of the service.
	 * @method Service#getAlt
	 * @returns {number} The alt group
	 */
	public getAlt() : number {
		return this.alt;
	}

	/**
	 * Set a new alt group for a service.
	 * @method Service#setAlt
	 * @param {number} newAlt The new alt group for the service
	 * @returns {Service} The updated Service
	 */
	public setAlt(newAlt : number) : Service {
		this.alt = newAlt;
		return this;
	}

	/**
	 * Get the sup group of the service.
	 * @method Service#getSup
	 * @returns {number} The sup group
	 */
	public getSup() : number {
		return this.sup;
	}

	/**
	 * Set a new sup group for a service.
	 * @method Service#setSup
	 * @param {number} newSup The new sup group for the service
	 * @returns {Service} The updated Service
	 */
	public setSup(newSup : number) : Service {
		this.sup = newSup;
		return this;
	}

	/**
	 * Get the array of sources of the service.
	 * @method Service#getSrcs
	 * @returns {Service[]} The array of sources
	 */
	public getSrcs() : Service[] {
		return this.src;
	}

	/**
	 * Set a new array of sources for a service.
	 * @method Service#setSrcs
	 * @param {Service[]} newSrcs The new array of sources for the service
	 * @returns {Service} The updated Service
	 */
	public setSrcs(newSrcs : Service[]) : Service {
		this.src = newSrcs;
		return this;
	}

	/**
	 * Add a new source in the array of sources of the service.
	 * @method Service#addSrc
	 * @param {Service} service The service to add to the array of sources
	 * @returns {Service} The updated Service
	 */
	public addSrc(service : Service) : Service {
		this.src.push(service);
		return this;
	}

	/**
	 * Remove all sources of the service.
	 * @method Service#removeSrcs
	 * @returns {Service} The updated Service
	 */
	public removeSrcs() : Service {
		this.src = [];
		return this;
	}

	/**
	 * Get the array of destinations of the service.
	 * @method Service#getDsts
	 * @returns {Service[]} The array of destinations
	 */
	public getDsts() : Service[] {
		return this.dst;
	}

	/**
	 * Set a new array of destinations for a service.
	 * @method Service#setDsts
	 * @param {Service[]} newDsts The new array of destinations for the service
	 * @returns {Service} The updated Service
	 */
	public setDsts(newDsts : Service[]) : Service {
		this.dst = newDsts;
		return this;
	}

	/**
	 * Add a new dstination in the array of dstinations of the service.
	 * @method Service#addDst
	 * @param {Service} service The service to add to the array of destinations
	 * @returns {Service} The updated Service
	 */
	public addDst(service : Service) : Service {
		this.dst.push(service);
		return this;
	}

	/**
	 * Remove all destinations of the service.
	 * @method Service#removeDsts
	 * @returns {Service} The updated Service
	 */
	public removeDsts() : Service {
		this.dst = [];
		return this;
	}

	/**
	 * Get the array of sources (control) of the service.
	 * @method Service#getSrcControls
	 * @returns {Service[]} The array of sources (control)
	 */
	public getSrcControls() : Service[] {
		return this.srcControl;
	}

	/**
	 * Set a new array of sources (control) for a service.
	 * @method Service#setSrcControls
	 * @param {Service[]} newSrcControls The new array of sources (control) for the service
	 * @returns {Service} The updated Service
	 */
	public setSrcControls(newSrcControls : Service[]) : Service {
		this.srcControl = newSrcControls;
		return this;
	}

	/**
	 * Add a new source (control) in the array of sources control of the service.
	 * @method Service#addSrcControl
	 * @param {Service} service The service to add to the array of sources (control)
	 * @returns {Service} The updated Service
	 */
	public addSrcControl(service : Service) : Service {
		this.srcControl.push(service);
		return this;
	}

	/**
	 * Remove all sources (control) of the service.
	 * @method Service#removeSrcControls
	 * @returns {Service} The updated Service
	 */
	public removeSrcControls() : Service {
		this.srcControl = [];
		return this;
	}

	/**
	 * Get the array of destinations (control) of the service.
	 * @method Service#getDstControls
	 * @returns {Service[]} The array of destinations (control)
	 */
	public getDstControls() : Service[] {
		return this.dstControl;
	}

	/**
	 * Set a new array of destinations (control) for a service.
	 * @method Service#setDstControls
	 * @param {Service[]} newDstControls The new array of destinations (control) for the service
	 * @returns {Service} The updated Service
	 */
	public setDstControls(newDstControls : Service[]) : Service {
		this.dstControl = newDstControls;
		return this;
	}

	/**
	 * Add a new destination (control) in the array of destinations control of the service.
	 * @method Service#addDstControl
	 * @param {Service} service The service to add to the array of destinations (control)
	 * @returns {Service} The updated Service
	 */
	public addDstControl(service : Service) : Service {
		this.dstControl.push(service);
		return this;
	}

	/**
	 * Remove all destinations (control) of the service.
	 * @method Service#removeDstControls
	 * @returns {Service} The updated Service
	 */
	public removeDstControls() : Service {
		this.dstControl = [];
		return this;
	}

	/**
	 * Get a specific element from the array of sources of the service.
	 * @method Service#getSrc
	 * @param {number} index The index of the element
	 * @returns {Service} The service placed in the position `index` of the array
	 */
	public getSrc(index : number) : Service {
		if(this.src.length <= index) {
			console.error("Bad index array when calling getSrc(index)");
			return null;
		}
		return this.src[index];
	}

	/**
	 * Set a new Service at a specific position of the array of sources of the service.
	 * @method Service#setSrc
	 * @param {number} index The index where the element will replace the old Service
	 * @param {Service} newService The new service
	 * @returns {Service} The updated service
	 */
	public setSrc(index : number, newService : Service) : Service {
		if(this.src.length <= index) {
			console.error("Bad index array when calling setSrc(index, newService)");
		} else {
			this.src[index] = newService;
		}
		return this;
	}

	/**
	 * Get a specific element from the array of destinations of the service.
	 * @method Service#getDst
	 * @param {number} index The index of the element
	 * @returns {Service} The service placed in the position `index` of the array
	 */
	public getDst(index : number) : Service {
		if(this.dst.length <= index) {
			console.error("Bad index array when calling getDst(index)");
			return null;
		}

		return this.dst[index];
	}

	/**
	 * Set a new Service at a specific position of the array of destinations of the service.
	 * @method Service#setDst
	 * @param {number} index The index where the element will replace the old Service
	 * @param {Service} newService The new service
	 * @returns {Service} The updated service
	 */
	public setDst(index : number, newService : Service) : Service {
		if(this.dst.length <= index) {
			console.error("Bad index array when calling setDst(index, newService)");
		} else {
			this.dst[index] = newService;
		}
		return this;
	}

	/**
	 * Get a specific element from the array of sources (control) of the service.
	 * @method Service#getSrcControl
	 * @param {number} index The index of the element
	 * @returns {Service} The service placed in the position `index` of the array
	 */
	public getSrcControl(index : number) : Service {
		if(this.srcControl.length <= index) {
			console.error("Bad index array when calling getSrcControl(index)");
			return null;
		}

		return this.srcControl[index];
	}

	/**
	 * Set a new Service at a specific position of the array of sources (control) of the service.
	 * @method Service#setSrcControl
	 * @param {number} index The index where the element will replace the old Service
	 * @param {Service} newService The new service
	 * @returns {Service} The updated service
	 */
	public setSrcControl(index : number, newService : Service) : Service {
		if(this.srcControl.length <= index) {
			console.error("Bad index array when calling setSrcControl(index, newService)");
		} else {
			this.srcControl[index] = newService;
		}
		return this;
	}

	/**
	 * Get a specific element from the array of destinations (control) of the service.
	 * @method Service#getDstControl
	 * @param {number} index The index of the element
	 * @returns {Service} The service placed in the position `index` of the array
	 */
	public getDstControl(index : number) : Service {
		if(this.dstControl.length <= index) {
			console.error("Bad index array when calling getDstControl(index)");
			return null;
		}

		return this.dstControl[index];
	}

	/**
	 * Set a new Service at a specific position of the array of destinations (control) of the service.
	 * @method Service#setDstControl
	 * @param {number} index The index where the element will replace the old Service
	 * @param {Service} newService The new service
	 * @returns {Service} The updated service
	 */
	public setDstControl(index : number, newService : Service) : Service {
		if(this.dstControl.length <= index) {
			console.error("Bad index array when calling setDstControl(index, newService)");
		} else {
			this.dstControl[index] = newService;
		}
		return this;
	}

	/**
	 * Get the description of the service.
	 * @method Service#getDescription
	 * @returns {string} The description of the service
	 *
	 */
	public getDescription() : string {
		return this.description;
	}

	/**
	 * Set the description of the service.
	 * @method Service#setDescription
	 * @param {string} newDescription The new description of the service
	 * @returns {Service} The updated service
	 *
	 */
	public setDescription(newDescription : string) : Service {
		this.description = newDescription;
		return this;
	}

	/**
	 * Remove service with a specific id from sources of the current service.
	 * If the service id does not exist, nothing happens.
	 * @method Service#removeServiceFromSrc
	 * @param {number} id The id of the service which will be removed from sources of the current service
	 * @returns {Service} The updated service
	 */
	public removeServiceFromSrc(id : number) : Service {
		var i = this.src.length;
		while(i--) {
			if(this.src[i].getId() == id) {
				this.src.splice(i, 1);
				console.log("Service with id: " + id + " has been removed from sources of the service with id: " + this.id);
				return this;
			}
		}
		return this;
	}

	/**
	 * Remove service with a specific id from destinations of the current service.
	 * If the service id does not exist, nothing happens.
	 * @method Service#removeServiceFromDst
	 * @param {number} id The id of the service which will be removed from destinations of the current service
	 * @returns {Service} The updated service
	 */
	public removeServiceFromDst(id : number) : Service {
		var i = this.dst.length;
		while(i--) {
			if(this.dst[i].getId() == id) {
				this.dst.splice(i, 1);
				console.log("Service with id: " + id + " has been removed from destinations of the service with id: " + this.id);
				return this;
			}
		}
		return this;
	}

	/**
	 * Remove service with a specific id from sources (control) of the current service.
	 * If the service id does not exist, nothing happens.
	 * @method Service#removeServiceFromSrcControl
	 * @param {number} id The id of the service which will be removed from sources (control) of the current service
	 * @returns {Service} The updated service
	 */
	public removeServiceFromSrcControl(id : number) : Service {
		var i = this.srcControl.length;
		while(i--) {
			if(this.srcControl[i].getId() == id) {
				this.srcControl.splice(i, 1);
				console.log("Service with id: " + id + " has been removed from sources (control) of the service with id: " + this.id);
				return this;
			}
		}
		return this;
	}

	/**
	 * Remove service with a specific id from destinations (control) of the current service.
	 * If the service id does not exist, nothing happens.
	 * @method Service#removeServiceFromDstControl
	 * @param {number} id The id of the service which will be removed from destinations (control) of the current service
	 * @returns {Service} The updated service
	 */
	public removeServiceFromDstControl(id : number) : Service {
		var i = this.dstControl.length;
		while(i--) {
			if(this.dstControl[i].getId() == id) {
				this.dstControl.splice(i, 1);
				console.log("Service with id: " + id + " has been removed from destinations (control) of the service with id: " + this.id);
				return this;
			}
		}
		return this;
	}
}
