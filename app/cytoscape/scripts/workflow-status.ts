/**
 * Enum which represents different possible status of the workflow.
 * @typedef {number} Enums.WorkflowStatus
 */
export enum WorkflowStatus {
	ON_CREATION,
	ON_UPDATE,
	STARTED,
	ON_EXECUTION,
	FAILED,
	FINISHED
}