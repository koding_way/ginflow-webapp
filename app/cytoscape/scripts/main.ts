// The usual bootstrapping imports
/**
 * Entry point of the application.
 */
import { bootstrap } from '@angular/platform-browser-dynamic';

import { AppComponent } from './components/app.component';

bootstrap(AppComponent);