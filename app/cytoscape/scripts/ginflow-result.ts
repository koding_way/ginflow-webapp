export class GinflowResult {
	/**
	 * The name of the {@link Service}.
	 * @member {string} GinflowResult#name
	 */
	name: string;

	/**
	 * The exit status of the {@link Service}.
	 * @member {string} GinflowResult#exit
	 */
	exit: string;

	/**
	 * The standard output of the {@link Service}.
	 * @member {string} GinflowResult#out
	 */
	out: string;

	/**
	 * The error output of the {@link Service}.
	 * @member {string} GinflowResult#err
	 */
	err: string;

	/**
	 * Constructor
	 * @class GinflowResult
	 * @classdesc Class which describe the result of a service.
	 * @param {string} name The name of the {@link Service}
	 * @param {string} exit The exit status of the {@link Service}
	 * @param {string} out The standard output of the {@link Service}
	 * @param {string} err The error output of the {@link Service}
	 */
	constructor(name : string, exit : string, out : string, err : string) {
		this.name = name;
		this.exit = exit;
		this.out = out;
		this.err = err;
	}
}