/**
 * Enum which concerns type of edges.
 * @typedef {number} Enums.TypeControl
 */
export enum TypeControl {
	Control,
	Data
}