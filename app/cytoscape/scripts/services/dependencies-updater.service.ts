import { Injectable }			from '@angular/core';

import { GraphService }			from './graph.service';

import { TypeControl }			from '../type-control';

@Injectable()
export class DependenciesUpdaterService {
	/**
	 * Constructor.
	 * Injects an instance of {@link GraphService}.
	 * @class DependenciesUpdaterService
	 * @classdesc Service which will take care of updating dependencies of services.
	 * (i.e. src, dst, src_control, dst_control)
	 * {@link GraphService} is used to get nodes and edges of the graph because this class owns the core instance of Cytoscape ({@link GraphService#cy}).
	 * @param {GraphService} graph the instance of {@link GraphService}
	 * @see GraphService
	 */
	constructor(private graph : GraphService) {}

	/**
	 * Remove all dependencies of all services in the graph.
	 * @method DependenciesUpdaterService#clearServiceDependencies
	 */
	public clearServiceDependencies() {
		// Loop over nodes of the graph
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			// Get the service of the current node
			var service = this.graph.getNodes()[i].data('service');

			// Remove all dependencies of this service (src, dst, src_control, dst_control)
			service .removeSrcs()
					.removeDsts()
					.removeSrcControls()
					.removeDstControls();
		}
	}

	/**
	 * Update dependencies of all services in the graph accordingly to their connected edges type (control or data).
	 * @method DependenciesUpdaterService#updateServiceDependencies
	 * @see Enums.TypeControl
	 */
	public updateServiceDependencies() {
		// Loop over edges of the graph
		for(var i = 0; i < this.graph.getEdges().length; i++) {

			// Get service of source node of the current edge
			var sourceNode = this.graph.getEdges()[i].source().data('service');

			// Get service of target node of the current edge
			var targetNode = this.graph.getEdges()[i].target().data('service');

			// Type of edge is 'Data'
			if(this.graph.getEdges()[i].data('type_control') == TypeControl.Data) {
				// We add the target node of this edge in the dst array of the source node
				sourceNode.addDst(targetNode);

				// We add the source node of this edge in the src array of the target node
				targetNode.addSrc(sourceNode);
			}

			// Type of edge is 'Control'
			else if(this.graph.getEdges()[i].data('type_control') == TypeControl.Control) {
				// We add the target node of this edge in the dst_control array of the source node
				sourceNode.addDstControl(targetNode);

				// We add the source node of this edge in the src_control array of the target node
				targetNode.addSrcControl(sourceNode);
			}
		}
	}

	/**
	 * Remove all dependencies which are alternative services in all services which ARE NOT alternatives.
	 * @method DependenciesUpdaterService#removeAlternativesServicesFromServicesDependencies
	 */
	public removeAlternativesServicesFromServicesDependencies() {
		for(var i = 0; i < this.graph.getNodes().length; i++) {
			var service = this.graph.getNodes()[i].data('service');
			if(service.alt == undefined || service.alt == "" || isNaN(service.alt)) {
				service.setSrcs(service.getSrcs().filter(function(s){return isNaN(s.alt)}));
				service.setDsts(service.getDsts().filter(function(d){return isNaN(d.alt)}));
				service.setSrcControls(service.getSrcControls().filter(function(s){return isNaN(s.alt)}));
				service.setDstControls(service.getDstControls().filter(function(d){return isNaN(d.alt)}));

			}
		}
	}
}
