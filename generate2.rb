#!/usr/bin/env ruby
#
require 'json'
require 'erb'

ENV['SRV'] ||= "echo"
ENV['IN'] ||= "1"

@horizontal = (ENV['HORIZONTAL'] || "1").to_i
@vertical = (ENV['VERTICAL'] || "1").to_i
@srv = ENV['SRV'] 
@in  = ENV['IN']
PAS_X = 300
PAS_Y = 150

def cytoscape(hash, index)
  h = ((index - 2 ).modulo(@horizontal) + 1) * PAS_X
  v = (((index - 2) / @vertical).floor + 1) * PAS_Y
  {
    :service => hash,
    :position => {
      :x => h, 
      :y => v
    }
  }
end

# first index
first_index = 1
first = cytoscape({
        :id => first_index.to_s(2),
        :name => first_index.to_s(2),
        :srv => @srv,
        :in => [@in],
        :src => [],
        :dst => (0..@horizontal - 1).map{|x| (x + first_index + 1).to_s(2)}}, 1)

last_index = @horizontal * @vertical + 2
last = cytoscape({
      :id => last_index.to_s(2),
      :name => last_index.to_s(2),
      :srv => @srv,
      :in => [last_index.to_s(2)],
      :src=>(0..@horizontal - 1).map{|x| (last_index - x - 1).to_s(2)},
      :dst => []}, last_index)
# last index

internals = (0..@vertical-1).map{ |i|
  (0..@horizontal-1).map { |j|
    index = i * @horizontal + j + 2 
    prec = index - @horizontal
    suiv = index + @horizontal
    s = {
      :id => index.to_s(2),
      :name => index.to_s(2),
      :srv => @srv,
      :in => [index.to_s]
    }
    # fill destinations
    if (i == @vertical - 1)
      s[:dst] = [(last_index).to_s(2)]
    else
      s[:dst] = [(suiv).to_s(2)]
    end
    # fill sources
    if (i == 0)
      s[:src] = [(first_index).to_s(2)]
    else
      s[:src] = [(prec).to_s(2)]
    end
    cytoscape(s, index)
  }
}

@first = first
@internals = internals.flatten
@last = last

services = [] + @internals
services << first
services << last

workflow = {
  :name => "wf-#{@horizontal}-#{@vertical}",
  :services => services
}

puts workflow.to_json

