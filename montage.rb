#!/usr/bin/env ruby
#
require 'json'
require 'erb'

OBJECT="m45"
DEGREE="0.5"
G5K=false
CLEAN=if G5K then "+8" else "+0" end
PAS_X = 300
PAS_Y = 75

def cytoscape(hash, i, j)
  {
    :service => hash,
    :position => {
      :x => i * PAS_X, 
      :y => j * PAS_Y
    }
  }
end

# first index

services =  []
mHdr = cytoscape({
        :id => "mHdr",
        :name => "mHdr",
        :srv => "mHdr",
        :in => ["#{OBJECT} #{DEGREE} template.hdr"],
        :dstControl => ["clean_template"],
}, 1, 1)
services << mHdr

bands = ["DSS2B", "DSS2R", "DSS2IR"]
clean = cytoscape({
          :id => "clean_template",
          :name => "clean_template",
          :srv => "tail",
          :in => ["-n #{CLEAN} template.hdr > template_clean.hdr"],
          :srcControl => ["mHdr"],
          :dstControl => bands.map{|band| "prepare_#{band}"}
  }, 1, 2)
services << clean

x=0
bands.each do |band|
    #mArchiveList dss DSS2B m45 1 1 rempte_DSS2B.tbl
  prepare = cytoscape({
          :id => "prepare_#{band}",
          :name => "prepare_#{band}",
          :srv => "mkdir raw_#{band} projected_#{band} || true",
          :in => [],
          :srcControl => ["clean_template"],
          :dstControl => ["mArchiveList_#{band}"]
  }, x, 3)
  services << prepare

  mArchiveList = cytoscape({
          :id => "mArchiveList_#{band}",
          :name => "mArchiveList_#{band}",
          :srv => "cd raw_#{band} ; mArchiveList",
          :in => ["dss #{band} #{OBJECT} #{DEGREE} #{DEGREE} remote_#{band}"],
          :srcControl => ["prepare_#{band}"],
          :dstControl => ["clean_#{band}"]
  }, x, 4)
  services << mArchiveList

  clean = cytoscape({
          :id => "clean_#{band}",
          :name => "clean_#{band}",
          :srv => "cd raw_#{band}; tail",
          :in => ["-n #{CLEAN} remote_#{band} > remote_#{band}_clean"],
          :srcControl => ["mArchiveList_#{band}"],
          :dstControl => ["mArchiveExec_#{band}"]
  }, x, 5)
  services << clean

  mArchiveExec = cytoscape({
          :id => "mArchiveExec_#{band}",
          :name => "mArchiveExec_#{band}",
          :srv => "cd raw_#{band}; mArchiveExec",
          :in => ["remote_#{band}_clean"],
          :srcControl => ["clean_#{band}"],
          :dstControl => []
  }, x, 6)
  services << mArchiveExec

  mImgTbl = cytoscape({
          :id => "mImgTbl_#{band}",
          :name => "mImgTbl_#{band}",
          :srv => "mImgtbl raw_#{band} rimages.tbl",
          :in => [],
          :srcControl => ["mArchiveExec_#{band}"],
          :dstControl => ["mProjExec_#{band}"]
  }, x, 7)
  services << mImgTbl


  mProj = cytoscape({
          :id => "mProjExec_#{band}",
          :name => "mProjExec_#{band}",
          :srv => "mProjExec -p raw_#{band} rimages.tbl template_clean.hdr projected_#{band} stats.tbl ",
          :in => [],
          :srcControl => ["mImgTbl_#{band}"],
          :dstControl => ["mImgTblp_#{band}"]
  }, x, 8)
  services << mProj


  mImgTblp = cytoscape({
          :id => "mImgTblp_#{band}",
          :name => "mImgTblp_#{band}",
          :srv => "mImgtbl projected_#{band} pimages.tbl",
          :in => [],
          :srcControl => ["mProjExec_#{band}"],
          :dstControl => ["mAdd_#{band}"]
  }, x, 9)
  services << mImgTblp

  mAdd = cytoscape({
          :id => "mAdd_#{band}",
          :name => "mAdd_#{band}",
          :srv =>"mAdd -p projected_#{band} pimages.tbl template_clean.hdr #{band}.fits",
          :in => [],
          :srcControl => ["mImgTblp_#{band}"],
          :dstControl => []
  }, x, 10)
  services << mAdd

  x = x + 1
end # band

mJpg = cytoscape({
        :id => "mJpg",
        :name => "mJpg",
        :srv =>"mJPEG -blue DSS2B.fits -1s 99.999% gaussian-log -green DSS2R.fits -1s 99.999% gaussian-log -red DSS2IR.fits -1s 99.999% gaussian-log -out DSS_BRIR.jpg",
        :in => [],
        :srcControl => bands.map{|band| "mAdd_#{band}"},
        :dstControl => []
}, 1, 11)
services << mJpg

workflow = {
  :name => "montage-#{OBJECT}-#{DEGREE}",
  :services => services
}

puts workflow.to_json


