#!/usr/bin/env ruby
#
require 'json'
require 'erb'

ENV['SRV'] ||= "echo"
ENV['IN'] ||= "1"

@horizontal = (ENV['HORIZONTAL'] || "1").to_i
@vertical = (ENV['VERTICAL'] || "1").to_i
@srv = ENV['SRV'] 
@in  = ""
PAS_X = 300
PAS_Y = 150


def cytoscape(hash, index, offsetx = 0, offsety = 0)
  h = ((index - 2 ).modulo(@horizontal) + 1) * PAS_X + offsetx
  v = (((index - 2) /  @horizontal).floor + 1) * PAS_Y + offsety
  hash[:id] = hash[:name]
  {
    :service => hash,
    :position => {
      :x => h, 
      :y => v
    }
  }
end
# first index
first_index = 1
first = cytoscape({
      :name => first_index.to_s,
      :srv => @srv,
      :in => [@in],
      :src => [],
      :dst => (0..@horizontal - 1).map{|x| (x + first_index + 1).to_s}
}, first_index)

last_index = @horizontal * @vertical + 2
last = cytoscape({
      :name => last_index.to_s,
      :srv => @srv,
      :in => [last_index.to_s],
      :src=>(0..@horizontal - 1).map{|x| (last_index - x - 1).to_s},
      :dst => []
}, last_index)

# last index
# generate a fully connected internals
precs = []
suivs = []
internals = (0..@vertical-1).map{ |i|
  (0..@horizontal-1).map { |j|
    index = i * @horizontal + j + 2 
    prec = index - @horizontal
    suiv = index + @horizontal
    precs = (prec..prec + @horizontal - 1).to_a.map{|x| x.to_s} if (j == 0)
    suivs = (suiv..suiv + @horizontal - 1).to_a.map{|x| x.to_s} if (j == 0)
    # index_v == i
    s = {
      :name => (index).to_s,
      :srv => @srv,
      :in => [""]
    }
    s[:sup] = "1" if (i == @vertical - 1 and j == @horizontal - 1)
    # fill destinations
    if (i == @vertical - 1)
      s[:dst] = [last_index.to_s]
    else
      s[:dst] = suivs
    end
    # fill sources
    if (i == 0)
      s[:src] = [first_index.to_s]
    else
      s[:src] = precs
    end
    cytoscape(s, index)
  }
}

# generate a simple connected alternatives
precs = []
suivs = []
alternatives = (0..@vertical-1).map{ |i|
  (0..@horizontal-1).map { |j|
    index = i * @horizontal + j + 2 
    prec = index - @horizontal + last_index
    suiv = index + @horizontal + last_index
    # index_v == i
    s = {
      :name => (index + last_index).to_s,
      :srv => @srv,
      :in => [""],
      :alt => "1"
    }
    # fill destinations
    if (i == @vertical - 1)
      s[:dst] = [last_index.to_s]
    else
      s[:dst] = [suiv]
    end
    # fill sources
    if (i == 0)
      s[:src] = [first_index.to_s]
    else
      s[:src] = [prec]
    end
    cytoscape(s, index, @horizontal * PAS_X, 0)
  }
}
@first = first
@internals = internals.flatten
@alternatives = alternatives.flatten
@last = last

services = [] + @internals + @alternatives
services << first
services << last

workflow = {
  :name => "wf-#{@horizontal}-#{@vertical}",
  :services => services
}

puts workflow.to_json

